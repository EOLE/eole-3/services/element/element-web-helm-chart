# Changelog

### [1.2.3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/compare/release/1.2.2...release/1.2.3) (2025-03-05)


### Bug Fixes

* **CI:** build helm chart automatically ([bc7d3f6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/bc7d3f6b701d061e3a435c4b05f9821c9d13dca2))
* new image version v1.11.85-eole3.0 ([fd8b27f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/fd8b27fc82894286c29a109503ccaf97e9a3c477))
* new image version v1.11.94-eole3.0 ([f02dbc2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/f02dbc2301b32b54d364c04fef94909d77022756))
* publish stable helm charts for v1.11.94-eole3.0 ([521fb7d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/521fb7da5a44f65429e9da523ee2392f991b9dc8))
* publish testing helm charts for v1.11.94-eole3.0 ([42e29ae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/42e29ae45e44ecc6629d851f00f802aeb3cb640b))

### [1.2.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/compare/release/1.2.1...release/1.2.2) (2024-11-13)


### Bug Fixes

* update repository path ([13bd605](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/13bd6054ddfc346a20712698f697d42b0c677e9b))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/compare/release/1.2.0...release/1.2.1) (2024-10-17)


### Bug Fixes

* update appVersion to 1.11.81-eole3.0 ([5399709](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/53997091bd5dc0e3a972a3e7b3730db593262dad))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/compare/release/1.1.1...release/1.2.0) (2024-06-10)


### Features

* customise login configuration ([2712cfb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/2712cfbe0859c285e79a41e28c0a5252963fcec5))
* deploy element-web 1.11.68-eole3.0 built from sources ([3778ba2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/3778ba270f84b751a3795e934f71d528ada4b11c))


### Bug Fixes

* invitation links are set to default element url ([88816d9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-helm-chart/commit/88816d9808b3daaf2528b0fc25477ea0350ff66b))

### [1.1.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/compare/release/1.1.0...release/1.1.1) (2024-05-03)


### Bug Fixes

* deploy image from eole registry by default ([47812e4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/47812e4bb7b35b11149202e314c5faa1fc481ca5))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-04-17)


### Features

* use own eole3 image ([2bc9f28](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/2bc9f28536d18bd4d6e2680bfce1e5e629aeda87))

## 1.0.0 (2024-04-16)


### Features

* **element:** first helm version for element ([2764679](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/2764679b50f7903a8eb16efeade9c2ea563cf7fc))
* first stable version ([34c6c66](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/34c6c66ed805b16b54a24712209bc85eb23aa22c))
* first testing version ([0e13032](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/0e130321b6de96d4a20d3cecbce93f0d5df4d0e3))


### Bug Fixes

* rename helm chart ([8fe1d50](https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-helm-chart/commit/8fe1d50acff14a981f13dfd12a70948b4bfbb69d))
